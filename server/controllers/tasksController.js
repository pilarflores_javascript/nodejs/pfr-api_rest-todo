const Task = require("../config/connectionDB").Task;
const express = require("express");

const app = express();



app.get("/tasks/:id", async(req, res) => {

    try {

        Task.findByPk(req.params.id).then(task => {
            if (!task) {
                return res.status(404).json({
                    ok: false,
                    error: "Object not found in Database."
                })
            }

            res.status(200).json(task)
        });


    } catch (error) {
        res.status(503).json(error)
    }



})

app.post("/tasks", async(req, res) => {
    try {
        const newTask = new Task({
            description: req.body.description,
            complete: req.body.complete,
        });

        await newTask.save();

        res.json({ task: newTask });

    } catch (error) {
        res.status(404).json({
            ok: false,
            error,
        })
    }
});


app.put("/tasks/:id", async(req, res) => {

    try {
        Task.update({
            complete: req.body.complete
        }, {
            where: { id: req.params.id }
        });

        res.status(204).json();

    } catch (error) {
        res.status(503).json(error)
    }

})

app.delete("/tasks/:id", async(req, res) => {

    try {
        Task.destroy({ where: { id: req.params.id } });

        res.status(204).json();

    } catch (error) {
        res.status(503).json(error)
    }

})


app.get("/tasks", async(req, res) => {

    try {
        Task.findAll().then(tasks => res.json(tasks))
    } catch (error) {
        res.status(503).json(error)
    }

})


module.exports = {
    app,
};