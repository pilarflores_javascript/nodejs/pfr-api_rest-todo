module.exports = (sequelize, type) => {
    return sequelize.define("tasks", {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        description: {
            type: type.STRING,
            demand: true,
            allowNull: false,
            unique: true,
        },
        complete: {
            type: type.BOOLEAN,
            defaultValue: false,
        },
    });
};