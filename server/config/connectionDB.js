const Sequelize = require("sequelize");

const TaskModel = require("../models/task");

const sequelize = new Sequelize("postgres_tasks", "root", "root", {
    host: "localhost",
    dialect: "postgres",
});

sequelize
    .authenticate()
    .then(() => {
        console.log("Connection has been established successfully.");
    })
    .catch((err) => {
        console.error("Unable to connect to the database:", err);
    });

sequelize.sync({ alter: true }).then(() => {
    console.log(`Database & tables update!`);
});


const Task = TaskModel(sequelize, Sequelize);

module.exports = {
    Task,
};